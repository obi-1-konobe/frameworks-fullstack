# Running application
Our application now has frontend talking to our backend api. Navigate to http://SERVER_IP:YOUR_PORT/
## Install requirements  
```
# Install pip
sudo apt-get install python3-pip
# Install requirements
sudo pip3 install -r requirements.txt
# Create ablemic table in db (on first run only)
flask db init
# Create migration file (if we changed model)
flask db migrate
# Apply migration to database
flask db upgrade
# Run tests
python3 tests.py
# Run app
flask run -- host=0.0.0.0 port=YOUR_PORT
```

# Frontend
```
# cd to frontend source folder
cd frameworks-fullstack/frontend

# Install dependances
npm install

# Run dev server, port will be written to console. Dev server supports hot reload
npm run dev

# Build bundle
npm run build
# Copy to flask
cp -R dist/* ../static/
```
